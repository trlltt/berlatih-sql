1. Membuat Database
Microsoft Windows [Version 10.0.18362.1016]
(c) 2019 Microsoft Corporation. All rights reserved.

C:\Users\ASUS>cd\

C:\>dir mysql
 Volume in drive C is OS
 Volume Serial Number is AA80-3540

 Directory of C:\

File Not Found

C:\>cd xampp

C:\xampp>cd mysql

C:\xampp\mysql>cd bin

C:\xampp\mysql\bin>mysql -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 60
Server version: 10.4.8-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.004 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| db_camat           |
| db_invenlab        |
| db_invenmuseum     |
| db_inventaris      |
| db_karyawan        |
| db_siteknik        |
| db_websekolah      |
| emuseum            |
| information_schema |
| myshop             |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
14 rows in set (0.001 sec)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
2. Membuat Table di Dalam Database
MariaDB [myshop]> create table users(
    -> id int auto_increment,
    -> primary key(id),
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255));
Query OK, 0 rows affected (0.041 sec)

MariaDB [myshop]> describe users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.048 sec)

MariaDB [myshop]> create table categories(
    -> id int auto_increment,
    -> primary key(id),
    -> name varchar(255));
Query OK, 0 rows affected (0.070 sec)

MariaDB [myshop]> describe categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.153 sec)

MariaDB [myshop]> create table items(
    -> id int auto_increment,
    -> primary key(id),
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> foreign key(category_id) references categories(id));
Query OK, 0 rows affected (0.058 sec)

MariaDB [myshop]> describe items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.043 sec)

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
3. Memasukkan Data pada Table
MariaDB [myshop]> insert into users(name,email,password)
    -> values("John Doe","john@doe.com","john123");
Query OK, 1 row affected (0.005 sec)

MariaDB [myshop]>  insert into users(name,email,password)
    -> values("Jane Doe","jane@doe.com","jenita123");
Query OK, 1 row affected (0.005 sec)

MariaDB [myshop]> select * from users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.000 sec)

MariaDB [myshop]> insert into categories(name) values("gadget");
Query OK, 1 row affected (0.119 sec)

MariaDB [myshop]> insert into categories(name) values("cloth");
Query OK, 1 row affected (0.024 sec)

MariaDB [myshop]> insert into categories(name) values("men");
Query OK, 1 row affected (0.005 sec)

MariaDB [myshop]> insert into categories(name) values("women");
Query OK, 1 row affected (0.008 sec)

MariaDB [myshop]> insert into categories(name) values("branded");
Query OK, 1 row affected (0.025 sec)

MariaDB [myshop]> select * from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.001 sec)

MariaDB [myshop]> insert into items(name,description,price,stock,category_id)
    -> values("Sumsang b50","hape keren dari merek sumsang",4000000,100,1);
Query OK, 1 row affected (0.006 sec)

MariaDB [myshop]> insert into items(name,description,price,stock,category_id)
    -> values("Uniklooh","baju keren dari brand ternama",500000,50,2);
Query OK, 1 row affected (0.030 sec)

MariaDB [myshop]> insert into items(name,description,price,stock,category_id)
    -> values("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);
Query OK, 1 row affected (0.006 sec)

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
4. Mengambil Data dari Database
a. Mengambil data users
MariaDB [myshop]> select name,email from users;
+----------+--------------+
| name     | email        |
+----------+--------------+
| John Doe | john@doe.com |
| Jane Doe | jane@doe.com |
+----------+--------------+
2 rows in set (0.001 sec)

b. Mengambil data items
MariaDB [myshop]> select * from items where price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.001 sec)

MariaDB [myshop]> select * from items where name like '%sang%';
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | price   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang | 4000000 |   100 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+
1 row in set (0.000 sec)

c. Menampilkan data items join dengan kategori
MariaDB [myshop]>  select items.name,items.description,items.price,items.stock,items.category_id,categories.name from items join categories on items.category_id=categories.id;
+-------------+-----------------------------------+---------+-------+-------------+--------+
| name        | description                       | price   | stock | category_id | name   |
+-------------+-----------------------------------+---------+-------+-------------+--------+
| Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget |
| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth  |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
+-------------+-----------------------------------+---------+-------+-------------+--------+
3 rows in set (0.001 sec)

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
5. Mengubah Data dari Database
MariaDB [myshop]> update items set price=2500000 where name="Sumsang b50";
Query OK, 1 row affected (0.026 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)